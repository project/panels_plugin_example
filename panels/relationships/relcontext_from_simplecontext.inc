<?php


/**
 * @file 
 *
 * Sample relationship plugin
 * 
 * This just gives a "relcontext" if it already has a "simplecontext"
 */

function panels_plugin_example_relcontext_from_simplecontext_panels_relationships() {
  $args['relcontext_from_simplecontext'] = array(
    'title' => t("Relcontext from Simplecontext"),
    'keyword' => 'relcontext',
    'description' => t('Adds a relcontext from existing simplecontext.'),
    'required context' => new panels_required_context(t('Simplecontext'), 'simplecontext'),
    'context' => 'panels_relcontext_from_simplecontext_context',
    'settings form' => 'panels_relcontext_from_simplecontext_settings_form',
    'settings form validate' => 'panels_relcontext_from_simplecontext_settings_form_validate',
  );
  return $args;
}

/**
 * Return a new context based on an existing context
 */
function panels_relcontext_from_simplecontext_context($context = NULL, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL
  if (empty($context->data)) {
    return panels_context_create_empty('relcontext', NULL);
  }

  // You can do error-checking heere
  
  // Create the new context from some element of the parent context.
  // In this case, we'll just use the parent context's argument
  return panels_context_create('relcontext',$context->argument);
  
}

/**
 * Settings form for the relationship
 */
function panels_relcontext_from_simplecontext_settings_form($conf) {


  return $form;
}

