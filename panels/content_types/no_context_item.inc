<?php


/**
 * @file
 * "No context" sample content type. It operates with no context at all.
 *
 * This example is to demo the simplest content type - one that needs no context
 * There is some information about the content_types plugin at
 * http://doxy.samboyer.org/panels2/panels_api_plugins_content_types.html
 *
 */

/**
 * Hook to supply a list of content types.
 * The hook is:
 * <modulename>_<content_type>_panels_content_types()
 */
function panels_plugin_example_no_context_item_panels_content_types() {
    // Key must be the name of the content type
  $items['no_context_item'] = array(
    // Unknown where this is used
    'title' => t('Panels Example No Context Item'),
    // Constructor?
    'content_types' => 'no_context_item',
    // Unknown what this means
    'single' => TRUE,
    // Name of a function which will render the block
    'render callback' => 'no_context_item_render',
    // Function providing the title for the
    'title callback' => 'no_context_item_title',
    // The form function that is used to config this content type
    'add callback' => 'no_context_item_admin',
    // Form function used to edit this type. Here we use the same one.
    'edit callback' => 'no_context_item_admin',
  );
  return $items;
}

/**
 * Constructor for the content type.
 *
 * @return unknown_type
 */
function no_context_item() {
  $item = array(
    'description' => array(
      // Used in the list of available panel items in the "add" screen
      'title' => t('No Context Item'),
      // goes in the directory with the content type. Here, in panels/content_types
      'icon' => 'icon_example.png',
      'path' => panels_get_path('content_types/no_context_item'),
      'description' => t('No Context Item - requires no context.'),
      // 'required context' => new panels_required_context(t('Junk'), 'junk'),
      'category' => array(t('Panels Examples'), -9),
    ),
  );
  return $item;
}

/**
 * Run-time rendering of the body of the blcok
 *
 * @param $subtype
 * @param $conf Configuration as done at admin time
 * @param $panel_args
 * @param $context  Context - in this case we don't have any
 *
 * @return unknown_type  An object with at least title and content members
 */
function no_context_item_render($subtype, $conf, $panel_args, $context) {
  $account = isset($context->data) ? drupal_clone($context->data) : NULL;
  $block = new stdClass();
  // $block->module = 'term-list';

  // The title actually used in rendering
  $block->title = check_plain("No-context item");
  $block->content = "This is some data for the body of the block.";
  $block->content .= "<br/>This was configured with configured with {$conf['item1']}, {$conf['item2']}, {$conf['item3']}";
  return $block;
}

/**
 * Display the administrative title for a panel pane in the drag & drop UI
 * This is only shown in the Panels UI
 */
function no_context_item_title($subtype, $conf, $context) {
  // The returned title is used on the admin page. It can be anything.
  // This shows using the contents of $conf to give more information
  return t('"no context" item configured w/@item1,@item2,@item3',
    array('@item1' => $conf['item1'], '@item2' => $conf['item2'], '@item3' => $conf['item3'])
  );;
}

/**
 * 'Edit' callback for the content type
 * This example just returns a form
 *
 */
function no_context_item_admin($id, $parents, $conf = array()) {
  // Apply defaults
  if (empty($conf)) {
      /* ... */
    $conf = array('item1' => 'default1', 'item2' => 'default2',
    );
  }

  $form['item1'] = array(
    '#type' => 'textfield',
    '#title' => t('Item1'),
    '#size' => 50,
    '#description' => t('The stuff for item 1.'),
    '#default_value' => $conf['item1'],
    '#prefix' => '<div class="clear-block no-float">',
    '#suffix' => '</div>',
  );
  $form['item2'] = array(
    '#type' => 'textfield',
    '#title' => t('Item2'),
    '#size' => 50,
    '#description' => t('The stuff for item 2'),
    '#default_value' => $conf['item2'],
    '#prefix' => '<div class="clear-block no-float">',
    '#suffix' => '</div>',
  );

  return $form;
}

