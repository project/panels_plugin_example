<?php


/**
 * @file
 * Sample panels2 content type that takes advantage of context
 *
 * This example uses the context it gets (simplecontext) to demo how a
 * panels2 content type can access and use context.
 *
 * There is some information about the content_types plugin at
 * http://doxy.samboyer.org/panels2/panels_api_plugins_content_types.html
 *
 * This example attempts to present a minimal
 */

/**
 * Hook to supply a list of content types.
 * The hook is:
 * <modulename>_<content_type>_panels_content_types()
 */
function panels_plugin_example_simplecontext_item_panels_content_types() {
    // Key must be the name of the content type
  $items['simplecontext_item'] = array(
    // Unknown where this is used
    'title' => t('Panels Example No Context Item'),
    // Constructor?
    'content_types' => 'simplecontext_item',
    // Unknown what this means
    'single' => TRUE,
    // Name of a function which will render the block
    'render callback' => 'simplecontext_item_render',
    // Function providing the title for the
    'title callback' => 'simplecontext_item_title',
    // The form function that is used to config this content type
    'add callback' => 'simplecontext_item_admin',
    // Form function used to edit this type. Here we use the same one.
    'edit callback' => 'simplecontext_item_admin',
  );
  return $items;
}

/**
 * Constructor for the content type.
 *
 * @return unknown_type
 */
function simplecontext_item() {
  $item = array(
    'description' => array(
      // Used in the list of available panel items in the "add" screen
      'title' => t('Simplecontext item'),
      // goes in the directory with the content type. Here, in panels/content_types
      'icon' => 'icon_example.png',
      'path' => panels_get_path('content_types/simplecontext_item'),
      'description' => t('Simplecontext - works with a simplecontext context.'),
      'required context' => new panels_required_context(t('Simplecontext'), 'simplecontext'),
      'category' => array(t('Panels Examples'), -9),
    ),
  );
  return $item;
}

/**
 * Run-time rendering of the body of the blcok
 *
 * @param $subtype
 * @param $conf Configuration as done at admin time
 * @param $panel_args
 * @param $context  Context - in this case we don't have any
 *
 * @return unknown_type  An object with at least title and content members
 */
function simplecontext_item_render($subtype, $conf, $panel_args, $context) {
  $data = $context->data;
  $block = new stdClass();
  // $block->module = 'term-list';

  // Don't forget to check this data if it's untrusted
  // The title actually used in rendering
  $block->title = "Simplecontext item";
  $block->content = "This is some data for the body of the block.";
  $block->content .= "<br/>This was configured with configured with <em>'{$conf['config_item_1']}'</em>";
  $block->content .= "<br/>And the context was <em>'{$context->data->description}'</em>";
  $block->content .= "<br/>And panel_args were ". print_r($panel_args, TRUE);
  return $block;
}

/**
 * Display the administrative title for a panel pane in the drag & drop UI
 * This is only shown in the Panels UI
 */
function simplecontext_item_title($subtype, $conf, $context) {
  // The returned title is used on the admin page. It can be anything.
  // This shows using the contents of $conf to give more information
  return t('Simplecontext: w/@item1',
    array('@item1' => $conf['config_item_1'],
    )
  );;
}

/**
 * 'Edit' callback for the content type
 * This example just returns a form
 *
 */
function simplecontext_item_admin($id, $parents, $conf = array()) {
  // Apply defaults
  if (empty($conf)) {
      /* ... */
    $conf = array('config_item_1' => 'default1',
    );
  }

  $form['config_item_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Config Item 1'),
    '#size' => 50,
    '#description' => t('The stuff for item 1.'),
    '#default_value' => $conf['config_item_1'],
    '#prefix' => '<div class="clear-block no-float">',
    '#suffix' => '</div>',
  );

  return $form;
}

