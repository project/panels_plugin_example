<?php


/**
 * @file
 *
 * Sample plugin to provide an argument handler for a simplecontext
 *
 * Given any argument to the panels page, simplecontext will get it
 * and turn it into a context.
 */

/**
 * Return settings for the simplecontext_arg
 * Name construction: <mymodule>_<arg_name>_panels_arguments()
 * 
 * @return unknown_type
 */
function panels_plugin_example_simplecontext_arg_panels_arguments() {
  $args['simplecontext_arg'] = array(
    'title' => t("Simplecontext arg"),
    // keyword to use for %substitution
    'keyword' => 'simplecontext',
    'description' => t('Creates a simplecontext from the arg.'),
    'context' => 'simplecontext_arg_context',
    'settings form' => 'simplecontext_arg_settings_form',
    'settings form submit' => 'simplecontext_arg_settings_form_submit',
    'displays' => 'simplecontext_arg_displays',
    'choose display' => 'simplecontext_arg_choose_display',
    // This has to map to a URL with an access callback
    'native path' => 'node',
    //'load function' => 'user',
  );
  return $args;
}

/**
 * Discover if this argument gives us the simplecontext we crave.
 */
function simplecontext_arg_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  // If $empty == TRUE it wants a generic, unfilled context.
  if ($empty) {
    return panels_context_create_empty('simplecontext');
  }

  // Do whatever error checking is required, returning PANELS_ARG_IS_BAD if it is
  // demo of a test for something that might be wrong. Normally you'd check
  // for a missing object, one you couldn't create, etc.
  if (drupal_strlen($arg) > 5) {
    return PANELS_ARG_IS_BAD;
  }

  return panels_context_create('simplecontext', $arg);
}

/**
 * Settings form for the argument
 */
function simplecontext_arg_settings_form($conf) {}

/**
 * There appears to be a bit of a bug with the way we're handling forms; it causes
 * 'checkboxes' to get invalid values added to them when empty. This takes care
 * of that.
 */
function simplecontext_arg_settings_form_submit(&$values) {}

/**
 * What additional displays does this argument provide?
 */
function simplecontext_arg_displays($conf, $id) {
  return array();
}

/**
 * Based upon the settings and the context, choose which display to use.
 */
function simplecontext_arg_choose_display($conf, $context) {}

