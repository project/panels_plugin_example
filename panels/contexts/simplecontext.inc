<?php


/**
 * @file
 * Sample panels2 context type plugin that shows how to create a context from an arg
 *
 * Plugin to provide a simplecontext context
 */
function panels_plugin_example_simplecontext_panels_contexts() {
  $args['simplecontext'] = array(
    'title' => t("Simplecontext"),
    'description' => t('A single simplecontext object.'),
    'context' => 'panels_plugin_example_context_create_simplecontext',
    'settings form' => 'simplecontext_settings_form',
    'keyword' => 'simplecontext',
    'context name' => 'simplecontext',
  );
  return $args;
}

/**
 * Create a context, either from manual configuration or from an argument on the URL
 *
 * @param $empty  If true, just return an empty context
 * @param $data   If from settings form, an array as from a form. If from argument, a string
 * @param $conf   TRUE if the $data is coming from admin configuration, FALSE if it's from a URL arg
 *
 * @return a Context object
 */
function panels_plugin_example_context_create_simplecontext($empty, $data = NULL, $conf = FALSE) {
  $context = new panels_context('simplecontext');
  $context->plugin = 'simplecontext';

  if ($empty) {
    return $context;
  }

  if ($conf) {
    if (!empty($data)) {
      $context->data = new stdClass();
      $context->data->description = "Our context is &gt;{$data['item1']}&lt; and it came from config info.";
      $context->title = t("Simplecontext Context from config");
      return $context;
    }
  }
  else {
    // $data is coming from an arg - it's just a string
    // This is used for keyword
    $context->title = $data;
    $context->argument = $data;
    // Make up a bogus context
    $context->data = new stdClass();
    $context->data->description = "Our context arg is >$data< as you can see and we got it from an arg";
    return $context;
  }
}

function simplecontext_settings_form($conf, $external = FALSE) {
  $form = array();
  $form['item1'] = array(
    '#type' => 'textfield',
    '#title' => t('Item1'),
    '#size' => 50,
    '#description' => t('The stuff for item 1.'),
    '#default_value' => '3',
    '#prefix' => '<div class="clear-block no-float">',
    '#suffix' => '</div>',
  );
  return $form;
}

